param($ReportDir,
      $SubscriptionsFile)

$MyDir = $(Get-Location).Path
Write-Host "Running under: $MyDir"

Write-Host "Detecting platform..."
if($PSVersionTable.Platform -eq "Unix") {
  Write-Host "PowerShell Unix identified."
  $Platform = "Unix"
}
else {
  Write-Host "PowerShell Desktop identified."
  $Platform = "Windows"
}

if(!$SubscriptionsFile) {
  Write-Error "`nNo subscriptions file given. Cannot get information about specific subscriptions without knowing them..."
  exit 1
}

if($ReportDir) {
  Write-Host "`nUsing the given report directory."
  $ReportPath = $ReportDir
}
else {
  Write-Host "`nNo report directory is given. Using default one."
  if($Platform -eq "Unix") {
    $ReportPath = "$HOME/AzureResourceInventory"
  }
  else {
    $ReportPath = "C:\AzureResourceInventory\"
  }
}
if(!(Test-Path $ReportPath)) {
  New-Item $ReportPath -ItemType Directory
}
Write-Host "Writing all results to: $ReportPath"

Write-Host "`nLoading subscriptions from $SubscriptionsFile"
$Subscriptions = Get-Content "$SubscriptionsFile"

Write-Host "`nRun ARI for $(($Subscriptions | Measure-Object -Line).Lines) subscriptions.`n"
foreach($Subscription in $Subscriptions) {
  if($Platform -eq "Unix") {
    $Job = Start-Job -WorkingDirectory $MyDir -ScriptBlock {./AzureResourceInventory.ps1 -SubscriptionID $args[0] -IncludeTags -ReportName $args[1] -ReportDir $args[2]} -ArgumentList $Subscription, "$Subscription", "$ReportPath"
  }
  else {
    $Job = Start-Job -WorkingDirectory $MyDir -ScriptBlock {.\AzureResourceInventory.ps1 -SubscriptionID $args[0] -IncludeTags -ReportName $args[1] -ReportDir $args[2]} -ArgumentList $Subscription, "$Subscription", "$ReportPath"
  }
  Wait-Job $Job
  Receive-Job $Job
}

